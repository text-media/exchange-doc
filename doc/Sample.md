# PHP

```php
$userId  = 123456;
$userKey = 'USER_API_KEY';

$request = [
    [
        "jsonrpc" => "2.0",
        "method" => "offer.getList",
        "id" => 0,
        "params" => [
            "limit" => 0,
            "offset" => 0,
            "sort" => [
                "name" => "Offer.modifiedAt",
                "dir" => "DESC"
            ],
            "performerId" => $userId,
            "exchangeType" => 1,
            "state" => [4]
        ]
    ]
];
$request = json_encode($request);

$context = stream_context_create(array(
    'http' => array(
        'method'           => 'POST',
        'protocol_version' => 1.1,
        'timeout'          => 10,
        'max_redirects'    => 2,
        'header'           => implode("\r\n", [
            'Content-type: application/x-www-form-urlencoded',
            'X-Sign: ' . hash('sha256', $request . $userKey),
            'X-Key: '  . $userKey
        ]),
        'content'          => $request,
        'ignore_errors'    => true,
    )
));

$stream = fopen('https://exchange.text.ru/api/performer', 'rb', false, $context);
if (!is_resource($stream)) {
    throw new Exception('Unable to establish a connection');
}

$response = stream_get_contents($stream);
$response = json_decode($response, true);

var_export($response);
```

# Js

Требуется [jQuery](https://jquery.com/) и функция для расчета `sha256`, например [https://geraintluff.github.io/sha256/sha256.min.js](https://geraintluff.github.io/sha256/sha256.min.js).

```js
var userId = 123456;
var userKey = "USER_API_KEY";

var request = [
    {
        "jsonrpc": "2.0",
        "method": "offer.getList",
        "id": 0,
        "params": {
            "limit": 0,
            "offset": 0,
            "sort": {
                "name": "Offer.modifiedAt",
                "dir": "DESC"
            },
            "performerId": userId,
            "exchangeType": 1,
            "state": [4]
        }
    }
];
request = JSON.stringify(request);

$.ajax({
    type:        "POST",
    headers:     {
        "X-Sign": sha256(request + userKey),
        "X-Key":  userKey
    },
    dataType:    "json",
    contentType: "application/json",
    url:         "https://exchange.text.ru/api/performer",
    data:        request,
    success:     (response) => {
        console.log(response);
    },
});
```
