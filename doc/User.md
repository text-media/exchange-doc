# User API

Запросы, не зависящие от типа пользователя.

Все запросы используют спецификацию [JSON-RPC 2.0](http://www.jsonrpc.org/specification).

URL для запросов: `/api/user`.

## user.getInfo

Получение информации о пользователях.

```json
{
    "jsonrpc": "2.0",
    "method": "user.getInfo",
    "params": {
        "list": [41539]
    },
    "id": 1
}
```

| Название | Обязательный | Содержимое               |
| -------- | ------------ | ------------------------ |
| `list`   | +            | Массив ID пользователей. |

Ответ содержит массив, ключами которого являются ID пользователя, а значениями - массив с информацией о данном пользователе как заказчике (`customer`), копирайтере (`copywriter`) и рерайтере (`rewriter`):

| Поле            | Тип       | Описание                              |
| --------------- | --------- | ------------------------------------- |
| `id`            | `integer` | ID пользователя.                      |
| `name`          | `string`  | Имя, указанное в профиле.             |
| `profile`       | `string`  | Ссылка на страницу профиля.           |
| `avatar`        | `string`  | Ссылка на аватар (может быть `NULL`). |
| `hasProAccount` | `boolean` | Имеет ли про-аккаунт.                 |
| `isBanned`      | `boolean` | Не забанен ли.                        |
| `isDeleted`     | `boolean` | Не удален ли.                         |
| `rating.value`  | `integer` | Накопленные очки рейтинга.            |
| `rating.level`  | `integer` | Уровень рейтинга (от 1 до 61).        |
| `rating.rank`   | `integer` | Ранг (от 1 до 8).                     |
| `rating.name`   | `string`  | Название ранга.                       |

Пример ответа:

```json
{
    "jsonrpc": "2.0",
    "result": {
        "41539": {
            "customer": {
                "id": 41539,
                "name": "Бармаглот",
                "profile": "https://text.ru/barmaglot",
                "avatar": "https://text.ru/images/avatars/10/0a/96-59b38d498ad2fcdf0b34d586dfb0100a.png",
                "hasProAccount": true,
                "isBanned": false,
                "isDeleted": false,
                "rating": {
                    "value": 130112,
                    "level": 23,
                    "rank": 5,
                    "name": "Крупный бизнесмен"
                }
            },
            "copywriter": {
                "id": 41539,
                "name": "jzucen",
                "profile": "https://text.ru/jzucen",
                "avatar": "https://text.ru/images/avatars/75/36/96-66b00ceae0a0359f4e7533b0dd437536.png",
                "hasProAccount": true,
                "isBanned": false,
                "isDeleted": false,
                "rating": {
                    "value": 1225,
                    "level": 2,
                    "rank": 1,
                    "name": "Школьник"
                }
            },
            "rewriter": {
                "id": 41539,
                "name": "jzucen",
                "profile": "https://text.ru/jzucen",
                "avatar": "https://text.ru/images/avatars/75/36/96-66b00ceae0a0359f4e7533b0dd437536.png",
                "hasProAccount": true,
                "isBanned": false,
                "isDeleted": false,
                "rating": {
                    "value": 960,
                    "level": 2,
                    "rank": 1,
                    "name": "Школьник"
                }
            }
        }
    },
    "id": 1
}
```

